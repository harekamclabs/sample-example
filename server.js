/**
 * Created by harekam on 16/09/15.
 */
'use strict';
const Hapi = require('hapi');
var Routes = require('./Routes');
var Plugins = require('./Plugins');
var config = require('./Config');
var log4js = require('log4js');
var logger = log4js.getLogger('[SERVER]');
var PORT = config.serverConfig.PORT.LIVE;
var mongoose = require('mongoose');
var MONGO_DB_URI = config.dbConfig.mongodbURI.local;
var Bootstrap = require('./Bootstrap');
/**
 * Server Config
 */
const server = new Hapi.Server();

if (process.env.NODE_ENV === 'TEST') {
    PORT = config.serverConfig.PORT.TEST;
} else if (process.env.NODE_ENV === 'DEV') {
    PORT = config.serverConfig.PORT.DEV;
}

var connectionOptions = {
    port: PORT,
    routes: {
        cors: true
    }
};
server.connection(connectionOptions);


/**
 * Plugins
 */
server.register(Plugins, function (err) {
    if (err) {
        server.error('Error while loading Plugins : ' + err)
    } else {
        server.log('info', 'Plugins Loaded')
    }


});

// API Routes
Routes.forEach(function (api) {
    server.route(api);
});

// Add the route
server.route({
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        reply('Welcome to Demo project!');
    }
});
//Connect to MongoDB
mongoose.connect(MONGO_DB_URI, function (err) {
    if (err) {
        server.log("DB Error: ", err);
        process.exit(1);
    } else {
        server.log('MongoDB Connected at', MONGO_DB_URI);
    }
});
// Start the server
server.start(function () {
    server.log('Server running at:', server.info.uri);
});

Bootstrap.init();
/**
 * For debugging
 */
if (process.env.LOCAL !== "TRUE") {
    server.on('response', function (request) {
        logger.debug('Request payload:', request.payload);
        logger.debug('Response payload in url', request.response.request.path, "with payload:", request.response.source);
    });
}
