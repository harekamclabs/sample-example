/**
 * Created by harekam on 27/08/15.
 */
'use strict';

const local = "mongodb://localhost/sample_example";

const mongodbURI = {
    local: local
};

module.exports = {
    mongodbURI: mongodbURI
};