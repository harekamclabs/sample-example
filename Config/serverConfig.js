/**
 * Created by harekam on 27/08/15.
 */

'use strict';

const PORT = {
    LIVE: 8000,
    DEV: 8080,
    TEST: 8888
};
const JWT_SECRET_KEY = '91tThEndsOfKeY';
const APP_NAME = 'Example';

module.exports = {
    JWT_SECRET_KEY: JWT_SECRET_KEY,
    APP_NAME: APP_NAME,
    PORT: PORT
};